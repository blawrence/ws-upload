#!/usr/bin/env python
import requests
import os


def send_to_api(json_report):
    url = os.environ['API_ENDPOINT']
    headers = os.environ['API_HEADERS']
    call = requests.post(url=url, headers=headers, data=json_report)
    return call.status_code
