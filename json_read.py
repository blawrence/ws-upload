import json, copy


def json_read(file_import):
    with open(file_import) as json_file:  
        json_data = json.load(json_file)
        json_file.close()
    with open(file_import) as json_file:  
        report_summary = json.load(json_file)
        json_file.close()

    output_data = []
    vulnerabilities = []
    #report_summary = copy.copy(json_data)
    del report_summary[0]['report_data']
    output_data.append(report_summary[0])
    

    for report_section in json_data[0]['report_data']:
        if any(term in report_section['name'] for term in ["Compliance", "Setup", "Appendix"]):
            continue
        for vuln in report_section['results']:
            try:
                    #fairly bad way of checking if there is a finding - errors and moves on if there isnt
                    print vuln['finding']['cvss']

                    vulnerabilities.append(vuln)
            except KeyError:
                pass
    output_data.append(vulnerabilities)

    #need to check and see if this is a new finding - also to take out old ones

    #could write these to a file for testing purposes
    with open('vulns.json', 'w+') as outfile:
        json.dump(output_data, outfile)
    
    #but we really want to send it back
    return output_data

json_read('./test.json')