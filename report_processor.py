#!/usr/bin/env python
import json, math, os
from os.path import isfile, getmtime, getctime
from random import randint
from datetime import date, datetime

def process_report(file_locations):
    modified_date = str(datetime.fromtimestamp(getmtime(file_locations)))
    create_date = str(datetime.fromtimestamp(getctime(file_locations)))
    low_vulns = 0
    med_vulns = 0
    high_vulns = 0
    with open(file_locations) as json_file:  
        new_json = json.load(json_file)
        json_file.close()

    new_json[0]['report_last_modified'] = modified_date
    new_json[0]['report_created'] = create_date
    #look for old file
    try:
        old_file = './reports/' + new_json[0]['package_name'] + "-" + new_json[0]['device_os'] + ".json"
    except KeyError:
        print file_locations + ' is not a valid report!'
        return "Error"

    #if there is a previous report, set to true to vuln id will be pulled rather than created
    previous_report = '{}'
    old_report = False
    if(isfile(old_file)):
        old_report = True
            #load previous report as old
        with open(old_file) as old_json:
            previous_report = json.load(old_json)
            old_json.close()        

    #iterate through report and assign vuln ids either from old report or create new ones
    report_index = -1
    for report_section in new_json[0]['report_data']:
        report_index += 1
        if any(term in report_section['name'] for term in ["Compliance", "Setup", "Appendix"]):
            continue
        #keeps track of which vuln we are currently on
        vuln_index = -1
        for vuln in report_section['results']:
            if vuln['status'] == 'low':
                low_vulns += 1
            if vuln['status'] == 'medium':
                med_vulns += 1
            if vuln['status'] == 'high':
                high_vulns += 1
            vuln_index += 1
            #checks for HTML output in the item - have to remove it because it messes with json interpretation - does not affect actualy vulnerabilities
            if '<table' in new_json[0]['report_data'][report_index]['results'][vuln_index]['description']:
                del new_json[0]['report_data'][report_index]['results'][vuln_index]
            #check to see if we need to make a new vuln ID or pull it from old report
            if old_report == False:
                new_json[0]['report_data'][report_index]['results'][vuln_index]['vuln_id'] = str(randint(10000000,99999999))
            else:
                #there is an old report, but have to check for new/different findings
                #if there is a vuln id already, copy it
                if 'vuln_id' in previous_report[0]['report_data'][report_index]['results'][vuln_index]:
                    new_json[0]['report_data'][report_index]['results'][vuln_index]['vuln_id'] = previous_report[0]['report_data'][report_index]['results'][vuln_index]['vuln_id']
                else:
                    new_json[0]['report_data'][report_index]['results'][vuln_index]['vuln_id'] = str(randint(10000000,99999999))
    new_json[0]['high_vulns'] = high_vulns
    new_json[0]['med_vulns'] = med_vulns
    new_json[0]['low_vulns'] = low_vulns
    if(old_report):
        os.remove(old_file)
    if not os.path.exists('./reports'):
        os.makedirs('./reports')
    new_file = open(old_file,"w+")
    new_file.write(json.dumps(new_json))
    new_file.close()
    

    return new_json

#locations = []
#locations.append('test.json')
#file = open('vuln_test.json','w')
#raw = process_report(locations)
#json_dump = json.dumps(raw)
#file.write(json_dump)

#print('done')