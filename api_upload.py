#!/usr/bin/env python
# Use the built-in version of scandir/walk if possible, otherwise
# use the scandir module version

import sys, getopt, os, json, logging
from datetime import date, datetime
from scandir import scandir
from report_processor import process_report
from api_processor import send_to_api

logFile_path = "./"
logFile_name = "logfile.txt"
logFile_full = logFile_path + logFile_name
scriptStartTime = datetime.now()
searchPath = "./"

debug = True

def log_debug(string):
    if debug == True:
        print(string)

def file_len(fname):
    with open(fname) as f:
        for i in enumerate(f):
            pass
    return i

def getLastTimeStamp(file_name):
    print "Opening log file at " + logFile_full
    logFile = open(logFile_full, "r")
    lines = logFile.readlines()
    lastLineIndex = file_len(logFile_full)
    log_debug("Log file last line is on row " + str(lastLineIndex[0]) + ":\n")
    log_debug("Last line: " + str(lastLineIndex[1]))
    return str(lastLineIndex[1])

def scantree(path):
    """Recursively yield DirEntry objects for given directory."""
    for entry in scandir(path):
        try:
            if entry.is_dir(follow_symlinks=False) and not "Library" in entry.path and not "/." in entry.path and not "./reports" in entry.path:
                for entry in scantree(entry.path):
                    yield entry
            else:
                yield entry
        except OSError:
            log_debug("Permission error")


def checkIsReport(filePath):
    try:
        with open(filePath) as json_file:  
            json_data = json.load(json_file)
   
        if "During setup, data population is performed on the device and several automated tests" in json_data[0]['report_data'][0]['description']:
            log_debug("Adding file" + filePath)
            return True
        else:
            return False
    except KeyError:
        log_debug("Check failed")
    except ValueError:    
        log_debug("Check failed")
    except TypeError:
        log_debug("Check failed")
    except IndexError:
        log_debug("Check failed")

try:
    sys.argv[1]
    searchPath = str(sys.argv[1])
    log_debug("Using directory path: " + searchPath)
except IndexError:
    log_debug("Using default path: /")
except NameError:
    log_debug("Using default path: /")

filesToUpload = []
lastLine = str(getLastTimeStamp(logFile_full))
log_debug(lastLine)
lastScan = datetime.strptime( lastLine , '%Y-%m-%d %H:%M:%S.%f\n')
logFile = open(logFile_full, "a")
for entry in scantree(searchPath):
    #print str(entry.stat().st_mtime)
    try:
        log_debug("Checking file: " + str(entry.name) + " with time " + str(datetime.fromtimestamp(entry.stat().st_mtime)))
        if datetime.fromtimestamp(entry.stat().st_mtime) > lastScan and ".json" in entry.name and entry.is_file():
            log_debug("Checking file: " + str(entry.path))
            if checkIsReport(entry.path):
                logFile.write(str(datetime.now()) + " Added file to upload: " + str(entry.path) + "\n")
                filesToUpload.append(entry.path)
    except OSError:
        log_debug("Found inaccessible file at" + entry.path)

for file_path in filesToUpload:
<<<<<<< HEAD
    processed_json_report = process_report(file_path)
    if not processed_json_report == "Error":
        print 'Success for ' + file_path
    else:
        logFile.write(str(datetime.now()) + ' Error while processsing: ' + file_path + ' is not a valid file\n')
    #log success status
    #if send_to_api == 200:
        #logFile.write('Successfully uploaded ' + file_path)
    #else:
        #logFile.write('Failed to upload ' + file_path)

#   with open(file_path) as json_file:  
#       json_data = json.load(json_file)
logFile.write(str(datetime.now()) + '\n')
=======
    with open(file_path) as json_file:  
        json_data = json.load(json_file)
        #would put HTTP call here



>>>>>>> origin/master
